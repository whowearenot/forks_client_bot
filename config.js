const path = require("path");

module.exports = {
    controller: "http://127.0.0.1:18312",
    token: "token",
    logs: {
        error: path.resolve(__dirname, "logs", "error.log"),
        combined: path.resolve(__dirname, "logs", "combined.log")
    },
    modules: path.resolve(__dirname, "modules")
};
