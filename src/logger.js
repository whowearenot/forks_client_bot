const config = require("../config");
const winston = require("winston");
const {
    combine,
    timestamp,
    json
} = winston.format;

const logger = winston.createLogger({
    level: "info",
    format: json(),
    transports: [
        new winston.transports.File({
            filename: config.logs.error,
            level: "error",
            maxsize: 1024 * 1024
        }),
        new winston.transports.File({
            filename: config.logs.combined,
            maxsize: 1024 * 1024
        })
    ]
});

if (process.env.NODE_ENV !== "production") {
    logger.add(new winston.transports.Console({
        format: winston.format.simple()
    }));
}

module.exports = logger;
