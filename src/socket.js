const IO = require("socket.io-client");
const EventEmitter = require("events");
const { Socket: IOSocket } = IO;
const { sleep } = require("./lib");

module.exports = class Socket extends EventEmitter {
    constructor({ url, token }) {
        super();
        this.url = url;
        this.token = token;

        this.handlers = [];
    }

    _createSocket() {
        /**
         * @type {IOSocket}
         */
        this.socket = new IO(this.url, {
            rejectUnauthorized: false,
            autoConnect: false,
            reconnection: false,
            timeout: 10000
        });
    }

    emit(...args) {
        this.socket.emit(...args);
    }

    async call(method, ...args) {
        return new Promise((resolve, reject) => {
            this.socket.emit(method, ...args, ({ error, result }) => {
                if (error) {
                    const err = new Error(error.message);
                    err.stack = error.stack;
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    }

    async handler(event, callback) {
        const listener = async function (...args) {
            const cb = args.length > 0 && typeof args[args.length - 1] === "function"
                ? args.pop()
                : () => {};
            try {
                const res = await callback(...args);
                cb({
                    error: null,
                    result: res
                });
            } catch (err) {
                console.log({
                    error: {
                        message: err.message,
                        stack: err.stack,
                        meta: err.meta
                    }
                });
                cb({
                    error: {
                        message: err.message,
                        stack: err.stack,
                        meta: err.meta
                    }
                });
            }
        };
        this.socket.on(event, listener);

        return {
            event,
            listener,
            socket: this.socket,
            remove() {
                this.socket.removeListener(this.event, this.listener);
            }
        };
    }

    async connect() {
        this._createSocket();
        for ( ; ; ) {
            this.socket.connect();
            try {
                await new Promise((resolve, reject) => {
                    const clean = () => {
                        this.socket.removeListener("connect", onConnected);
                        this.socket.removeListener("connect_error", onError);
                        this.socket.removeListener("connect_timeout", onError);
                    };
                    const onConnected = () => {
                        clean();
                        resolve();
                    };
                    const onError = (error) => {
                        clean();
                        reject(new Error(`Could not connect: ${error.message}`));
                    };
                    this.socket.on("connect", onConnected);
                    this.socket.on("connect_error", onError);
                    this.socket.on("connect_timeout", onError);
                });
                break;
            } catch (err) {
                super.emit("connectError", err);
                await sleep(2500);
            }
        }
        try {
            await this.call("clientBot.register", {
                token: this.token
            });
        } catch (err) {
            await this.disconnect();
            throw err;
        }
        this.socket.on("disconnect", () => {
            super.emit("disconnect");
        });
    }

    async disconnect() {
        this.socket.disconnect();
    }
};
