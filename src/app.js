const logger = require("./logger");
const config = require("../config");
const Socket = require("./socket");
const ModuleManager = require("./module_manager");

class App {
    constructor() {
        this.socket = null;
        this.moduleManager = null;
        this.controller = null;
        this.uninitializing = false;
    }

    async run() {
        try {
            await this.initialize();
            return await this.main();
        } catch (err) {
            logger.error(`could not start the app: ${err.stack}`);
            return 1;
        }
    }

    async connect() {
        await this.disconnect();
        this.socket = new Socket({
            url: config.controller,
            token: config.token
        });
        this.socket.on("connectError", (error) => {
            logger.error(error.message);
        });
        await this.socket.connect();

        this.socket.on("disconnect", () => {
            if (!this.uninitializing) {
                this.socket = null;
                this.reinitialize();
            }
        });
    }

    async disconnect() {
        if (!this.socket) {
            return;
        }
        await new Promise((resolve) => {
            this.socket.disconnect();
            this.socket.once("disconnect", resolve);
        });
    }

    setState(state) {
        if (this.socket) {
            this.socket.emit("state", state);
        }
    }

    async initialize() {
        logger.info("initializing");
        await this.connect();
        this.setState("INITIALIZING");
        this.moduleManager = new ModuleManager({
            socket: this.socket,
            path: config.modules
        });

        await this.moduleManager.initialize();
        this.socket.emit("state", "READY");
        logger.info("initialized");
    }

    async unitialize() {
        logger.info("uninitializing");
        this.setState("UNINITIALIZING");
        this.uninitializing = true;
        await this.moduleManager.uninitialize();
        await this.disconnect();
        this.uninitializing = false;
        this.setState("UNINITIALIZED");
        logger.info("uninitialized");
    }

    async reinitialize() {
        logger.info("reinitialize");
        try {
            await this.unitialize();
        } catch (err) {
            logger.error(`could not uninitialize: ${err.stack}`);
            await new Promise((resolve) => setTimeout(resolve, 1000));
            process.exit(1);
        }
        try {
            await this.initialize();
        } catch (err) {
            logger.error(`could not uninitialize: ${err.stack}`);
            await new Promise((resolve) => setTimeout(resolve, 1000));
            process.exit(1);
        }
    }

    async main() {
        logger.info("it works");
    }
}

new App().run().then((x) => {
    if (typeof x === "number") {
        process.exit(x);
    }
});
