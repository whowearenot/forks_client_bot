const tar = require("tar");
const fs = require("mz/fs");
const path = require("path");
const EventEmitter = require("events");
const Socket = require("./socket");
const SocketStream = require("socket.io-stream");
const os = require("os");
const del = require("del");
const mkdirp = require("mkdirp-promise");
const execa = require("execa");

module.exports = class ModuleMangaer extends EventEmitter {
    constructor({ path, socket }) {
        super();
        this.path = path;

        /**
         * @type {Socket}
         */
        this.socket = socket;
        this.modules = new Map();
        this.handlers = [];

        SocketStream(socket.socket).on("modules.file", (stream, data) => {
            this.emit("file", stream, data);
        });
    }

    async initialize() {
        await this.serverSync();
        await this.localSync();
    }

    async handler(event, callback) {
        const handler = await this.socket.handler(event, callback);
        this.handlers.push(handler);
    }

    async localSync() {
        const filenames = await fs.readdir(this.path);
        let loaded = 0;
        let unloaded = 0;
        let reloaded = 0;
        for (const name of filenames) {
            if (this.modules.has(name)) {
                const module = this.modules.get(name);
                const newVersion = await this.getVersion(name);
                if (newVersion !== module.version) {
                    await this.unloadModule(name);
                    await this.loadModule(name);
                    ++reloaded;
                }
            } else {
                await this.loadModule(name);
                ++loaded;
            }
        }
        const filenamesSet = new Set(filenames);
        for (const name of this.modules.keys()) {
            if (!filenamesSet.has(name)) {
                await this.unloadModule(name);
                ++unloaded;
            }
        }
        return {
            loaded,
            unloaded,
            reloaded
        };
    }

    getPath(name) {
        return path.resolve(this.path, name);
    }

    async getVersion(name) {
        const modulePath = this.getPath(name);
        try {
            const config = JSON.parse(await fs.readFile(path.resolve(modulePath, "package.json")));
            return config.version;
        } catch (err) {
            if (err.code === "ENOENT") {
                return null;
            }
            throw err;
        }
    }

    async getConfig(name) {
        const modulePath = this.getPath(name);
        return JSON.parse(await fs.readFile(path.resolve(modulePath, "config.json")));
    }

    async loadModule(name) {
        const modulePath = this.getPath(name);
        delete require.cache[modulePath];
        const Module = require(modulePath);
        const module = new Module(this.socket);
        await module.initialize();
        this.modules.set(name, module);
    }

    async unloadModule(name) {
        if (!this.modules.has(name)) {
            return;
        }
        const module = this.modules.get(name);
        this.modules.delete(name);
        await module.uninitialize();
    }

    async reloadModule(name) {
        await this.unloadModule(name);
        await this.loadModule(name);
    }

    async uninitialize() {
        for (const handler of this.handlers) {
            handler.remove();
        }
        for (const name of this.modules.keys()) {
            await this.unloadModule(name);
        }
    }

    async serverSync() {
        const serverModules = await this.socket.call("modules.getAll");
        for (const serverModule of serverModules) {
            const localVersion = await this.getVersion(serverModule.name);
            if (localVersion !== serverModule.version) {
                await this.install(serverModule.name);
            }
        }
    }

    async install(name) {
        const stream = await new Promise((resolve) => {
            const handler = (stream, data) => {
                if (data.name === name) {
                    this.removeListener("file", handler);
                    resolve(stream);
                }
            };
            this.on("file", handler);
            this.socket.emit("modules.download", name);
        });
        const tmpdir = await fs.mkdtemp(path.resolve(os.tmpdir(), "forks-tmp-"));
        try {
            const tarPath = path.resolve(tmpdir, "module.tar.gz");
            await new Promise((resolve, reject) => {
                stream.pipe(fs.createWriteStream(tarPath)).on("finish", resolve).on("error", reject);
                stream.on("error", reject);
            });
            const destPath = path.resolve(this.path, name);
            await del(destPath, { force: true });
            await mkdirp(destPath);
            await tar.extract({
                file: tarPath,
                cwd: destPath
            });
            const installScript = path.resolve(destPath, "install.sh");
            await fs.chmod(installScript, 0o755);
            await execa.shell(installScript, {
                cwd: destPath
            });
        } finally {
            await del(tmpdir, { force: true });
        }
    }
};
